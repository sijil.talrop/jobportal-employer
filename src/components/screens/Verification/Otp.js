import {
  Animated,
  Image,
  SafeAreaView,
  Text,
  View,
  Platform,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

const {Value, Text: AnimatedText} = Animated;
const CELL_COUNT = 4;
export const CELL_SIZE = 70;
export const CELL_BORDER_RADIUS = 8;
export const DEFAULT_CELL_BG_COLOR = '#fff';
export const NOT_EMPTY_CELL_BG_COLOR = '#3557b7';
export const ACTIVE_CELL_BG_COLOR = '#f7fafe';
const {height, width} = Dimensions.get('window');
const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({hasValue, index, isFocused}) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const Otp = ({route}) => {
  const navigation = useNavigation();
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const renderCell = ({index, symbol, isFocused}) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [NOT_EMPTY_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [CELL_SIZE, CELL_BORDER_RADIUS],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1],
          }),
        },
      ],
    };

    setTimeout(() => {
      animateCell({hasValue, index, isFocused});
    }, 0);

    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}>
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    );
  };

  const [otp, setOtp] = useState('');
  const verifyOtp = () => {
    let post_url = base.BASE_URL + 'users/verify-otp/';
    console.log(post_url);

    axios
      .post(post_url, {
        phone: route.params.phone,
        otp: otp,
      })
      .then((response) => {
        let {StatusCode, data} = response.data;

        if (StatusCode == 6000) {
          let user = {
            access_token: response.data.data.data.access,

            // access_token:
            //   'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjUwMDk1ODEzLCJqdGkiOiJkMTIwOTVkZDRiYTM0NTU5YTYxY2ExYWNmOTcy...',
            role: 'user',
          };
          addAccount(user);
          console.warn(data);
        } else {
          Toast.show(data.message, Toast.SHORT);
          // console.warn(data.message);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const addAccount = async (user) => {
    console.log(user);
    await AsyncStorage.setItem('user', JSON.stringify(user)).then((result) => {
      Toast.show('Successfully Registered', Toast.SHORT);
      navigation.navigate('AccountTab', {
        phone: route.params.phone,
      });
    });
  };

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.imageBox}>
        <Image
          style={styles.icon}
          source={require('../../../assets/vector-images/Group20398.png')}
        />
      </View>
      <Text style={styles.title}>OTP Verification</Text>
      <Text style={styles.subTitle}>
        Enter the OTP sent to {route?.params.phone}
      </Text>
      <CodeField
        ref={ref}
        {...props}
        value={otp}
        onChangeText={setOtp}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={renderCell}
      />
      <Text style={styles.resendText}>
        Don't receive the OTP?<Text style={styles.option}>RESEND OTP</Text>
      </Text>
      <TouchableOpacity
        onPress={() => {
          verifyOtp();
        }}
        style={styles.nextButton}>
        <Text style={styles.nextButtonText}>Verify & proceed</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  codeFieldRoot: {
    height: CELL_SIZE,
    marginTop: 30,
    justifyContent: 'center',
  },
  cell: {
    marginHorizontal: 8,
    height: CELL_SIZE,
    width: CELL_SIZE,
    lineHeight: CELL_SIZE - 5,
    ...Platform.select({web: {lineHeight: 65}}),
    fontSize: 30,
    textAlign: 'center',
    borderRadius: CELL_BORDER_RADIUS,
    color: '#3759b8',
    backgroundColor: '#fff',
    // IOS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    // Android
    elevation: 3,
  },
  // =======================
  root: {
    minHeight: '100%',
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  title: {
    color: '#000',
    fontSize: 25,
    fontWeight: '700',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  imageBox: {
    width: width * 0.8,
    height: width * 0.8,
  },
  icon: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  resendText: {
    fontSize: 16,
    color: '#7b7b7b',
    marginTop: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  option: {
    color: '#ef801f',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  subTitle: {
    marginTop: 10,
    color: '#7b7b7b',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  nextButton: {
    marginTop: 30,
    borderRadius: 15,
    height: 60,
    backgroundColor: '#ef801f',
    justifyContent: 'center',
    width: width * 0.8,
  },
  nextButtonText: {
    textAlign: 'center',
    fontSize: 20,
    color: '#fff',
    fontWeight: '700',
  },
});

export default Otp;
