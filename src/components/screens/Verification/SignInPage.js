import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  TextInput,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
var {height, width} = Dimensions.get('window');
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';
import Toast from 'react-native-simple-toast';

const SignInPage = (props) => {
  // textinput Props and functions start
  const [phone, setPhone] = useState();
  const [isFocused, setIsFocused] = useState({
    phone: false,
  });
  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };
  const bottomValue = useRef(new Animated.Value(100)).current;
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 9000,
    }).start();
  };
  useEffect(() => {
    jumpUp();
  });
  // textinput Props and functions end Here

  const navigation = useNavigation();

  const loginNumber = () => {
    let post_url = base.BASE_URL + 'users/sign-in/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: phone,
      })
      .then((response) => {
        console.log(response.data);
        if (response.data.StatusCode == 6000) {
          // setTimeout(() => {
          //   setLoading(false);
          // }, 500);
          // navigation.navigate('Verification', {phone: phone});
          console.log('successs');
          props.navigation.navigate('OtpPage', {phone: phone});

          // save user data
        } else {
          // setTimeout(() => {
          //   setLoading(false);
          // }, 500);
          console.log('errooorrr');

          // Toast.show('Please try again later', Toast.SHORT);
          // console.warn(response.data.message);
        }
      })
      .catch((error) => {
        console.warn(error);
        console.log('catch erroorr');
      });
  };

  // useEffect(() => {
  //   loginNumber();
  // }, []);

  return (
    <>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group2039.png')}
              style={styles.image}
            />
          </View>
          <Animated.View
            style={[
              styles.box,
              {
                transform: [{translateY: bottomValue}],
              },
            ]}>
            <View style={styles.middle}>
              <Text style={styles.errorText}>OTP Verification</Text>
              <Text style={styles.content}>
                We will send an one Time Password {'\n'} on this mobile number
              </Text>
            </View>
            <View>
              <Text style={styles.number}>Enter Number</Text>
              <View
                style={[
                  styles.inputBox,
                  {borderColor: isFocused.phone ? '#ef801f' : '#aaa'},
                ]}>
                <TextInput
                  style={styles.input}
                  underlineColorAndroid="transparent"
                  placeholder="Mobile Number"
                  keyboardType="number-pad"
                  placeholderTextColor={isFocused.name ? '#000' : '#aaa'}
                  autoCapitalize="none"
                  onFocus={() => {
                    handleInputFocus('phone');
                  }}
                  onBlur={() => {
                    handleInputBlur('phone');
                  }}
                  onPress={() => {
                    setPhone();
                  }}
                  onChangeText={(val) => {
                    setPhone(val);
                  }}
                />
                <Icon name={'phone-outline'} color={'#4eeb12'} size={20} />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                if (phone?.length == 10) {
                  loginNumber();
                } else {
                  Toast.show('Please enter valid mobile number', Toast.SHORT);
                }
              }}
              activeOpacity={0.8}
              style={styles.button}>
              <Text style={styles.buttonText}>Get OTP</Text>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  contentBox: {
    justifyContent: 'flex-end',
    minHeight: '100%',
    alignItems: 'center',
    paddingBottom: 50,
  },
  imageContainer: {
    width: width * 0.9,
    height: width * 0.9,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
  middle: {
    paddingHorizontal: 40,
  },
  errorText: {
    fontSize: 26,
    color: '#000',
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    textAlign: 'center',
  },
  content: {
    fontSize: 20,
    textAlign: 'center',
    color: '#757575',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  middle: {
    width: width,
    padding: 40,
  },
  box: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
    marginTop: 20,
    width: width * 0.9,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 16,
    color: '#fff',
  },
  loginUser: {
    borderColor: '#ef801f',
    marginBottom: 20,
    width: width * 0.9,
  },
  input: {
    width: width * 0.6,
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderWidth: 1,
    width: width * 0.9,
    borderRadius: 15,
  },
  number: {
    fontSize: 19,
    textAlign: 'center',
    marginBottom: 10,
    color: '#757575',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
});

export default SignInPage;
