import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
var {height, width} = Dimensions.get('window');
import {CommonActions, useNavigation} from '@react-navigation/native';

const LogoScreen = ({route}) => {
  setTimeout(() => {
    navigation.navigate('IntroPage');
  }, 1000);
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.container}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group9570.png')}
              style={styles.image}
            />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentBox: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    width: width * 0.7,
    height: width * 0.7,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
});

export default LogoScreen;
