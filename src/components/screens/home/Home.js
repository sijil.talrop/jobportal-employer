import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Dimensions, Text, ScrollView} from 'react-native';
import CommonSearchBar from './CommonSearchBar';
import Category from './Category';
import HomeHeader from '../header/HomeHeader';
import Package from './Package';
import VacancyTab from '../recent-vacancy/VacancyTab';
import {ImageBackground} from 'react-native';

const {height, width} = Dimensions.get('window');

export default function Home(props) {
  const category = [{}];

  return (
    <>
      <ImageBackground
        style={styles.backgroundImageBox}
        source={require('../../../assets/vector-images/Group20260.png')}>
        <ScrollView
          contentContainerStyle={{
            alignItems: 'center',
            width: width,
            minHeight: '100%',
            paddingTop: '10%',
          }}>
          <View style={{}}>
            <View style={{paddingHorizontal: 15}}>
              <HomeHeader />
            </View>
            <View style={styles.topView}>
              <View>
                <Text style={styles.header}>Hello Antonio</Text>
                <Text style={styles.description}>Find Your Perfect Job</Text>
              </View>
            </View>
            <View>
              <View style={{marginBottom: 20}}>
                <CommonSearchBar />
              </View>
              <Category />
            </View>
            <View style={{marginTop: 20}}>
              <Package />
            </View>
            <View style={{marginTop: 20}}>
              <VacancyTab />
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    fontSize: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  backgroundImageBox: {
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width,
    height: height,
  },
  description: {
    fontSize: 25,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  topView: {
    paddingHorizontal: 15,
    paddingVertical: 29,
  },
});
