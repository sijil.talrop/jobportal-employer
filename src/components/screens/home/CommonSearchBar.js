import SearchBar from 'react-native-dynamic-search-bar';
import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import {TouchableOpacity} from 'react-native-gesture-handler';
const {width, height} = Dimensions.get('window');

export default function CommonSearchBar(props) {
  const [spinnerVisibility, setSpinnerVisibility] = useState(false);
  const [last_name, setLast_name] = useState('Last Name');
  const [finalQuery, setQuery] = useState('');

  const confirmHandler = (value) => {
    setSpinnerVisibility(true);
  };

  return (
    <View style={styles.middleContent}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}>
        <View style={styles.searchBox}>
          <SearchBar
            textInputStyle={{fontSize: 15}}
            onRequestSearch={() => setQuery()}
            fontColor="#fff"
            iconColor="#fdfdfd"
            shadowColor="yellow"
            cancelIconColor="#fdfdfd"
            // spinnerVisibility={spinnerVisibility}
            placeholder="What are you looking for?"
            fontFamily="BalooPaaji2-SemiBold"
            shadowStyle={{}}
            onChangeText={() => {
              confirmHandler();
            }}
            onSearchPress={() => last_name()}
            // spinnerColor="red"
            style={{
              backgroundColor: '#f3f5f8',
              height: 55,
              width: width * 0.75,
              borderRadius: 10,
              borderWidth: 1,
              elevation: 1,
              borderColor: '#eee',
            }}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('FilterPage');
          }}
          style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../../assets/vector-images/filter.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  imageContainer: {
    width: 50,
    height: 50,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  searchBox: {
    width: width * 0.78,
  },
});
