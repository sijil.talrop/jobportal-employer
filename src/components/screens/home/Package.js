import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ListItem} from 'react-native-elements';

const {width, height} = Dimensions.get('window');
const box_width = width - 40;
const box_height = box_width * 0.6;

export default function Package() {
  const packages = [
    {
      detail: '100 Candidates Cv',
    },
    {
      detail: 'Duis Vestibulum elit vel neque pharetra',
    },
    {
      detail: 'Excepteur sint occaecat cupidatat non',
    },
    {
      detail: '1sunt in culpa qui officia deserunt mollit anim ',
    },
  ];

  const renderItem = (props) => {
    return (
      <TouchableOpacity activeOpacity={0.9} style={styles.mainContent}>
        <ImageBackground
          source={require('../../../assets/vector-images/Group20260.png')}
          style={styles.bannerContainer}>
          <View style={styles.topContent}>
            <View style={styles.imgContainer}>
              <Image
                style={styles.image}
                source={require('../../../assets/vector-images/twcoe7t5.png')}
              />
            </View>
            <View style={styles.detailBox}>
              <Text style={styles.rightContentText}>Premium</Text>
              <Text style={styles.rightContentText}>
                ₹2500/<Text style={styles.lightContent}>month</Text>
              </Text>
            </View>
          </View>
          {packages.map((item, index) => (
            <View key={index} style={styles.locationBox}>
              <Icon name={'check'} color="#2261a6" size={20} />
              <Text style={[styles.locationText]}>{item.detail}</Text>
            </View>
          ))}
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}> Upgrade</Text>
          </TouchableOpacity>
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{}}>
      <View style={styles.HeaderContainer}>
        <Text style={styles.header}>Package</Text>
      </View>
      <View style={{alignItems: 'center'}}>{renderItem()}</View>
    </View>
  );
}
const styles = StyleSheet.create({
  HeaderContainer: {
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  buttonBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: width * 0.2,
    paddingVertical: 5,
    borderWidth: 0.5,
    borderColor: '#eee',
  },
  header: {
    fontSize: 22,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  topContent: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  imgContainer: {
    width: box_width * 0.16,
    height: box_width * 0.16,
    borderRadius: 10,
  },
  image: {
    width: null,
    height: null,
    borderRadius: 10,
    overflow: 'hidden',
    flex: 1,
  },
  mainContent: {
    width: box_width,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#aaa',
  },
  bannerContainer: {
    borderRadius: 30,
    padding: 10,
    overflow: 'hidden',
    padding: 20,
  },
  locationText: {
    fontSize: 16,
    fontFamily: 'BalooPaaji2-Regular',
    marginLeft: 10,
    color: '#9491a7',
  },
  locationBox: {
    flexDirection: 'row',
  },
  rightContentText: {
    fontSize: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  lightContent: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  detailBox: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 10,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 15,
  },
  buttonText: {
    fontSize: 16,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
  },
});
