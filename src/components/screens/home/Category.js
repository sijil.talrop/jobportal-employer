import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, Dimensions} from 'react-native';
var {height, width} = Dimensions.get('window');

const Category = () => {
  const [selectedRating, setSelectedRating] = useState('Product');
  const confirmHandler = (value) => {
    setSelectedRating(value);
  };

  const category = [
    {
      name: 'Product',
    },
    {
      name: 'Design',
    },
    {
      name: 'Development',
    },
  ];

  return (
    <>
      <TouchableOpacity style={styles.flexBox}>
        {category.map((item, index) => (
          <TouchableOpacity
            key={index}
            activeOpacity={0.8}
            onPress={() => {
              confirmHandler(item.name);
            }}
            style={[
              styles.mainContainer,
              {
                borderColor: item.name === selectedRating ? '#2261a6' : '#aaa',
              },
            ]}>
            <Text
              style={[
                styles.itemFont,
                {color: item.name === selectedRating ? '#2261a6' : '#aaa'},
              ]}>
              {item.name}
            </Text>
          </TouchableOpacity>
        ))}
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
    width: width,
    paddingHorizontal: 20,
  },
  itemFont: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#9ba0ab',
    textAlign: 'center',
    fontSize: 15,
  },
  mainContainer: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderRadius: 10,
    margin: 5,
    width: width * 0.27,
    backgroundColor: '#fff',
    borderWidth: 1,
    elevation: 4,
    borderColor: '#eee',
  },
});

export default Category;
