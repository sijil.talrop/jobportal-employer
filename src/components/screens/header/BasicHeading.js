import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');

export default function BasicHeadings(props) {
  useEffect(() => {
    console.warn(props.title);
  }, []);
  return (
    <View style={styles.iconBox}>
      <TouchableOpacity activeOpacity={8} style={styles.menu}>
        <Icon name="chevron-left" size={30} color={'#000'} />
      </TouchableOpacity>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: width * 0.5,
        }}>
        <Text style={styles.headText}>{props.title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  headText: {
    fontSize: 25,
    color: '#000',
    fontFamily: 'BalooPaaji2-Bold',
  },
  menu: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
});
