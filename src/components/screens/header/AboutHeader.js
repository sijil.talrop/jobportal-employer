import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');

export default function AboutHeader(props) {
  useEffect(() => {
    console.warn(props.title);
  }, []);
  return (
    <View style={styles.iconBox}>
      <TouchableOpacity activeOpacity={0.8} style={styles.menu}>
        <Icon name="chevron-left" size={24} color={'#000'} />
      </TouchableOpacity>
      {props.logo == true ? (
        <View style={styles.rightBox}>
          <TouchableOpacity activeOpacity={0.8} style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/vector-images/label.png')}
            />
          </TouchableOpacity>
        </View>
      ) : (
        <View style={[styles.rightBox, {backgroundColor: '#eee'}]}>
          <TouchableOpacity activeOpacity={0.8} style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/vector-images/label-1.png')}
            />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headText: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'Poppins-SemiBoldItalic',
  },
  menu: {
    width: 50,
    height: 50,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#eee',
    elevation: 1,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    resizeMode: 'contain',
  },
  imageBox: {
    width: 40,
    height: 40,
    borderColor: '#eee',
    overflow: 'hidden',
  },
  rightBox: {
    padding: 5,
    backgroundColor: '#fff',
    borderRadius: 10,
  },
});
