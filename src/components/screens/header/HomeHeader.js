import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');

export default function HomeHeader(props) {
  useEffect(() => {
    console.warn(props.title);
  }, []);

  return (
    <View style={styles.iconBox}>
      <TouchableOpacity
        onPress={() => {
          navigation.openDrawer();
        }}
        activeOpacity={0.8}
        style={styles.menu}>
        <Icon name="menu" size={24} color={'#000'} />
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} style={styles.imageBoxLog}>
        <Image
          style={styles.image}
          source={require('../../../assets/vector-images/x_logo.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} style={styles.imageBox}>
        <Image
          style={styles.image}
          source={require('../../../assets/vector-images/profile.jpg')}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headText: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'Poppins-SemiBoldItalic',
  },
  menu: {
    width: 50,
    height: 50,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#eee',
    elevation: 1,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 15,
    overflow: 'hidden',
  },
  imageBox: {
    width: 50,
    height: 50,
    borderWidth: 0.5,
    borderColor: '#eee',
    borderRadius: 15,
    overflow: 'hidden',
    elevation: 1,
  },
  imageBoxLog: {
    width: 40,
    height: 40,
  },
});
