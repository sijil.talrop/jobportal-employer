import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import CreateAccount from './CreateAccount';
import CreateEmployer from './CreateEmployer';
const {height, width} = Dimensions.get('window');

export default function AccountTab(props) {
  const [index, setIndex] = React.useState(0);
  const Tab = [
    {
      Box: 'Candidate',
    },
    {
      Box: 'Employer',
    },
  ];

  const renderTabList = () => {
    if (selectedBox == 'Candidate') {
      return <CreateAccount />;
    } else if (selectedBox == 'Employer') {
      return <CreateEmployer />;
    }
  };

  const [selectedBox, setSelectedBox] = useState(Tab[0].Box);
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };
  return (
    <ScrollView contentContainerStyle={styles.topView}>
      <View style={{alignItems: 'center'}}>
        <View style={styles.logoImage}>
          <Image
            style={styles.image}
            source={require('../../../assets/vector-images/Group9570.png')}
          />
        </View>
        <View>
          <Text style={styles.header}>Create Account</Text>
        </View>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  backgroundColor:
                    item.Box === selectedBox ? '#ef801f' : '#fff',
                },
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#fff' : '#000'},
                ]}>
                {item.Box}
              </Text>
            </TouchableOpacity>
          ))}
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.contentScroll}>
          {renderTabList()}
        </ScrollView>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  topView: {
    minHeight: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    marginTop: '10%',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    elevation: 3,
    marginVertical: 20,
  },
  tabBox: {
    backgroundColor: 'red',
    width: width * 0.4,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 10,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  logoImage: {
    width: width * 0.5,
    height: width * 0.15,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
});
