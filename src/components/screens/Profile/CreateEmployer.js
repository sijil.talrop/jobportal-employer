import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import {CommonActions, useNavigation} from '@react-navigation/native';
const {width, height} = Dimensions.get('window');

export default function CreateEmployer(props) {
  const navigation = useNavigation();
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [business, setBusiness] = useState('');
  const [phone, setPhone] = useState('');
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const [country, setCountry] = useState('');
  const [state, setState] = useState('');
  const [city, setCity] = useState('');
  const [location, setLocation] = useState('');
  const [category, setCategory] = useState('');
  const [country_list, setCountry_list] = useState();
  const [state_list, setState_list] = useState();
  const [city_list, setCity_list] = useState();
  const [location_list, setLocation_list] = useState();
  const [category_list, setCategory_list] = useState();

  useEffect(() => {
    get_country_list();
    get_state_list();
    get_city_list();
    get_location_list();
    get_category_list();
  }, []);

  const get_country_list = () => {
    let post_url = base.BASE_URL + 'cities/counties/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCountry_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_state_list = () => {
    let post_url = base.BASE_URL + 'cities/states/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setState_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_city_list = () => {
    let post_url = base.BASE_URL + 'cities/cities/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCity_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_location_list = () => {
    let post_url = base.BASE_URL + 'cities/locations/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setLocation_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  const get_category_list = () => {
    let post_url = base.BASE_URL + 'jobs/job-categories/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCategory_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const registration = () => {
    let post_url = base.BASE_URL + 'users/register/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: phone,
        first_name: first_name,
        last_name: last_name,
        business: business,
        country: country,
        state: state,
        city: city,
        location: location,
        category: category,
        user_type: 'moLv8xgjHYOF8mSvZCui0az2y4fjedu1',
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        // let {StatusCode} = response.data.StatusCode;
        // let {data} = response.data.data;
        if (StatusCode == 6000) {
          console.warn('created');
          setTimeout(() => {
            navigation.navigate('SuccessPopup');
          }, 500);
        } else {
          // Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    <View style={styles.middleContent}>
      <View style={styles.loginUser}>
        <TextInput
          value={first_name}
          style={styles.input}
          placeholder="First Name"
          // ref={SSSS}
          returnKeyType={'next'}
          onChangeText={(val) => setFirst_name(val)}
          // secureTextEntry={true}
        />
        <Icon name={'account'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={last_name}
          style={styles.input}
          placeholder="Last Name"
          // ref={ref_input2}
          returnKeyType={'next'}
          onChangeText={(val) => setLast_name(val)}
          // secureTextEntry={true}
        />
        <Icon name={'account'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={business}
          style={styles.input}
          placeholder="Business"
          // ref={ref_input2}
          returnKeyType={'next'}
          onChangeText={(val) => setBusiness(val)}
          // secureTextEntry={true}
        />
        <Icon name={'account'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={phone}
          style={styles.input}
          placeholder="Phone Number"
          // ref={ref_input3}
          returnKeyType={'next'}
          onChangeText={(val) => setPhone(val)}
          // secureTextEntry={true}
        />
        <Icon name={'phone'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={country}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}>
          {country_list &&
            country_list.length > 0 &&
            country_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={state}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setState(itemValue)}>
          {state_list &&
            state_list.length > 0 &&
            state_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={city}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCity(itemValue)}>
          {city_list &&
            city_list.length > 0 &&
            city_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={location}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}>
          {location_list &&
            location_list.length > 0 &&
            location_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
          {category_list &&
            category_list.length > 0 &&
            category_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <TouchableOpacity
        onPress={() => {
          if (first_name.length >= 3) {
            registration();
          } else {
            Toast.show('Please enter name perfectly', Toast.SHORT);
          }
        }}
        style={styles.login}>
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  middleContent: {
    marginTop: width * 0.04,
    width: width * 0.9,
  },
  inputContent: {
    paddingVertical: height * 0.01,
  },
  input: {
    fontSize: 15,
    width: width * 0.7,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  loginUser: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
  },
  picker: {
    fontSize: 10,
    color: '#aaa',
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
});
