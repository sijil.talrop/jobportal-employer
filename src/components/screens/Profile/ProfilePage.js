import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import EducationComponent from '../Profile/EducationComponent';
const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function MatchedJobs() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <ScrollView contentContainerStyle={styles.mainContainer}>
      <View>
        <View
          style={{
            backgroundColor: '#fff',
            alignItems: 'center',
          }}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={require('../../../assets/vector-images/suvitha.jpg')}
            />
          </View>
          <Text style={styles.name}>Dilsha Deva</Text>
          <Text style={styles.subText}>UI UX Designer</Text>
          <Text style={styles.mail}>Muthumani.talrop@gmai.com</Text>
        </View>
        <View style={styles.topBox}>
          <View style={styles.iconContainer}>
            <View style={styles.iconBox}>
              <Image
                style={styles.iconImage}
                source={require('../../../assets/vector-images/shield.png')}
              />
            </View>
            <Text style={styles.iconHead}>Experience</Text>
            <Text style={styles.detailBox}>
              3<Text style={{fontSize: 13}}>Years</Text>
            </Text>
          </View>
          <View style={styles.iconContainer}>
            <View style={styles.iconBox}>
              <Image
                style={styles.iconImage}
                source={require('../../../assets/vector-images/certificate.png')}
              />
            </View>
            <Text style={styles.iconHead}>Qualification</Text>
            <Text style={styles.detailText}>Bachelor Degree</Text>
          </View>
          <View style={styles.iconContainer}>
            <View style={styles.iconBox}>
              <Image
                style={styles.iconImage}
                source={require('../../../assets/vector-images/gender.png')}
              />
            </View>
            <Text style={styles.iconHead}>Gender</Text>
            <Text style={styles.detailText}>Male</Text>
          </View>
        </View>
        <View style={styles.inputBox}>
          <Icon name={'folder-download'} color={'#130937'} size={35} />
          <View style={styles.contentRightBox}>
            <Text style={styles.BottomContentHead}>Download CV</Text>
            <Text style={styles.BottomText}>Last updated on 20 Jan 2020</Text>
          </View>
        </View>
        <View style={styles.aboutContainer}>
          <Text style={styles.aboutHead}>About</Text>
          <Text style={styles.Description}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          </Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => setIsOpen(!isOpen)}
            style={styles.readMore}>
            <Text style={styles.read}>Read More</Text>
            <Icon
              name={isOpen ? 'chevron-up' : 'chevron-down'}
              color={'orange'}
              size={35}
            />
          </TouchableOpacity>
        </View>
        {isOpen && (
          <React.Fragment>
            <View style={styles.component}>
              <EducationComponent />
            </View>
          </React.Fragment>
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    backgroundColor: '#fff',
    minHeight: '100%',
    width: width,
    paddingTop: '15%',
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#fad4b2',
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 20,
    textTransform: 'capitalize',
    color: '#130937',
    marginTop: 10,
  },
  subText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    color: '#959595',
  },
  mail: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#959595',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
  topBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    paddingHorizontal: 20,
  },
  iconContainer: {
    alignItems: 'center',
    width: width * 0.3,
    height: width * 0.32,
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
  },
  iconBox: {
    width: width * 0.1,
    height: width * 0.1,
  },
  iconImage: {
    width: null,
    height: null,
    flex: 1,
  },
  iconHead: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 16,
    color: '#615b79',
  },
  detailBox: {
    fontSize: 18,
  },
  detailText: {
    fontSize: 16,
    color: '#362c55',
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    paddingHorizontal: 50,
  },
  contentRightBox: {
    marginLeft: 30,
  },
  BottomContentHead: {
    fontSize: 18,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#130937',
  },
  BottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    color: '#959595',
  },
  aboutContainer: {
    marginTop: 20,
    paddingHorizontal: 50,
  },
  aboutHead: {
    fontSize: 17,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  Description: {
    color: '#3d345b',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  readMore: {
    width: width - 40,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.2,
    borderRadius: 8,
    borderColor: '#eee',
    elevation: 0.5,
    paddingVertical: 10,
    marginVertical: 25,
  },
  read: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    color: '#afadbd',
  },
  component: {},
});
