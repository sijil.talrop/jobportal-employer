import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Progress from '../Profile/Progress';

const {width, height} = Dimensions.get('window');

export default function EducationComponent() {
  const renderQualification = [
    {
      college_name: 'mms Public and  college',
    },
    {
      college_name: 'mms Public and  college',
    },
  ];

  const image = [
    {
      star: 'head',
      image: 'https://www.inshatech.com/client-images/mockup-apex-small.jpg',
    },
    {
      star: 'Dude',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiy_8hU0niMSsyJEm-pxXmo6KXDkEHuNKBQ&usqp=CAU',
    },
    {
      star: 'Dude',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTn_jkOrxvGo8sTuAiBfZS7siJ2eKX-C1nGdQ&usqp=CAU',
    },
    {
      star: 'Dude',
      image: 'https://wbbn.ru/wp-content/uploads/2019/05/11.jpg',
    },
  ];

  const company = [
    {
      star: 'head',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSls94o-g8AQfCl17-wfl8VUOg68a2zzipcA7RNhUPWg8JVVJ4lGE8PkIgttEugT4AJg4E&usqp=CAU',
    },
    {
      star: 'Dude',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQx7UULq4nSCesr0WytADr5qgcxG60QXLlqcvAMWP4wF-xufpAGNIAn67bgiwpvnovN6vU&usqp=CAU',
    },
    {
      star: 'Dude',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFsuSUUOUjE9gA4qfDB3igffP2ydaDbKPh-aFPVQnJLV6e0SxhA59Ysq4tuNV99HQcjcc&usqp=CAU',
    },
    {
      star: 'Dude',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYkGoAcoEEZUSEgXHAAPPvPGe836DOLj5iyx2QTVzsHg14jbTmRQcHC90Lj1364ALTKPs&usqp=CAU',
    },
  ];

  const workExperience = () => {
    return (
      <View style={{}}>
        <Text style={styles.Header}>Work & Experience</Text>
        {renderQualification.map((item, index) => (
          <View
            key={index}
            style={[styles.mainFlexBox, {paddingHorizontal: 10}]}>
            <View style={{marginRight: 20}}>
              <Text style={{color: '#3681ba', fontSize: 26}}>○</Text>
            </View>
            <View style={{}}>
              <View style={styles.rightContent}>
                <Text style={styles.optionText}>
                  Perfect Attendance Programs
                </Text>
                <Text style={styles.yearText}>2012-2020</Text>
                <Text style={styles.description}>
                  The passage experienced a surge in popularity during the 1960s
                  when Letraset used it on their dry-transfer sheets, and again
                  during the
                </Text>
              </View>
            </View>
          </View>
        ))}
      </View>
    );
  };
  const ProtFolio = () => {
    return (
      <ScrollView
        pagingEnabled
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollStyle}
        onScroll={''}>
        {image.map((item, index) => (
          <TouchableOpacity key={index} style={{margin: 15}}>
            <View style={styles.imageContent}>
              <Image style={styles.image1} source={{uri: item.image}} />
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };

  const Awards = () => {
    return (
      <View style={{}}>
        <Text style={styles.Header}>Awards</Text>
        {renderQualification.map((item, index) => (
          <View
            key={index}
            style={[styles.mainFlexBox, {paddingHorizontal: 10}]}>
            <View style={{marginRight: 20}}>
              <Text style={{color: '#3681ba', fontSize: 26}}>○</Text>
            </View>
            <View style={{}}>
              <View style={styles.rightContent}>
                <Text style={styles.optionText}>
                  Perfect Attendance Programs
                </Text>
                <Text style={styles.yearText}>2012-2020</Text>
                <Text style={styles.description}>
                  The passage experienced a surge in popularity during the 1960s
                  when Letraset used it on their dry-transfer sheets, and again
                  during the
                </Text>
              </View>
            </View>
          </View>
        ))}
      </View>
    );
  };
  const Companies = () => {
    return (
      <ScrollView
        pagingEnabled
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollStyle}
        onScroll={''}>
        {company.map((item, index) => (
          <TouchableOpacity
            key={index}
            style={{marginHorizontal: width * 0.02}}>
            <View style={styles.companyImage}>
              <Image style={styles.image1} source={{uri: item.image}} />
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };
  return (
    <View style={{paddingLeft: 20}}>
      <Text style={styles.Header}>Education</Text>
      {renderQualification.map((item, index) => (
        <View key={index} style={styles.mainFlexBox}>
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/vector-images/cap.png')}
            />
          </View>
          <View style={styles.rightContent}>
            <Text style={styles.yearText}>2012-2020</Text>
            <Text style={styles.optionText}>
              Luis College
              <Text style={styles.yearText}>Bachelors in Fine Arts</Text>
            </Text>
            <Text style={styles.description}>
              The passage experienced a surge in popularity during the 1960s
              when Letraset used it on their dry-transfer sheets, and again
              during the
            </Text>
          </View>
        </View>
      ))}
      {workExperience()}
      <View>
        <Text style={styles.Header}>Protfolio</Text>
        {ProtFolio()}
      </View>
      <View style={{marginVertical: 20}}>
        <Text style={styles.Header}> Personal Skill</Text>
        <Progress title="Adobe PhotoShop" percentage={71} />
        <Progress title="Production in Html" percentage={64} />
        <Progress title="Graphic Design" percentage={85} />
      </View>
      {Awards()}
      <View>
        <Text style={styles.Header} s>
          Companies Followed By
        </Text>
        {Companies()}
      </View>

      <View style={{height: height * 0.12}}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  imageBox: {
    width: 40,
    height: 40,
    marginRight: 20,
  },
  scrollStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // width: width,
    // width: '100%',
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
  },
  mainFlexBox: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  rightContent: {
    width: width * 0.75,
  },
  yearText: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#9491a7',
  },
  optionText: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#3681ba',
  },
  description: {
    fontFamily: 'BalooPaaji2-Regular',
    color: '#42395f',
    fontSize: 15,
  },
  Header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    marginBottom: 10,
    color: '#130937',
  },
  imageContent: {
    width: width * 0.4,
    height: width * 0.3,
    backgroundColor: '#fff',
  },
  companyImage: {
    width: width * 0.32,
    height: width * 0.3,
  },
});
