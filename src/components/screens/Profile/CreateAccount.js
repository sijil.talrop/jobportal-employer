import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import {useNavigation} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');

export default function CreateAccount(props) {
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [email, setEmail] = useState('');
  const [re_enter_password, setRe_enter_password] = useState('password');
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();
  const ref_input5 = useRef();
  const [language, setLanguage] = useState('');

  const picker1 = [
    {
      id: 1,
      label: 'Type of Organaisation',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: 'Face Cream 33f',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: 'Face Cream 55k',
      value: 'wwwwwwwwww',
    },
  ];
  const picker2 = [
    {
      id: 1,
      label: 'Country',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: 'Sirach Heman',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: 'Ziyoka Dekko',
      value: 'wwwwwwwwww',
    },
  ];

  const navigation = useNavigation();

  return (
    <View style={styles.middleContent}>
      <View style={styles.loginUser}>
        <TextInput
          value={first_name}
          style={styles.input}
          placeholder="Company Name "
          ref={ref_input2}
          returnKeyType={'next'}
          onChangeText={(val) => setFirst_name(val)}
          // secureTextEntry={true}
        />
        <Icon name={'account-outline'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={last_name}
          style={styles.input}
          placeholder="phone Number"
          ref={ref_input3}
          returnKeyType={'next'}
          onChangeText={(val) => setLast_name(val)}
          // secureTextEntry={true}
        />
        <Icon name={'phone-outline'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={email}
          style={styles.input}
          placeholder="Email ID"
          ref={ref_input3}
          returnKeyType={'next'}
          onChangeText={(val) => setEmail(val)}
          // secureTextEntry={true}
        />
        <Icon name={'email-outline'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={language}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) =>
            setLanguage({language: itemValue})
          }>
          {picker2.map((item, index) => (
            <Picker.Item key={index} label={item.label} value={item.value} />
          ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={language}
          style={styles.picker}
          onValueChange={(itemValue) => setLanguage(itemValue)}>
          {picker1.map((item, index) => (
            <Picker.Item key={index} label={item.label} value={item.value} />
          ))}
        </Picker>
      </View>
      <TouchableOpacity
        style={styles.login}
        onPress={() => {
          navigation.navigate('SuccessPopUp');
        }}>
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  middleContent: {
    marginTop: width * 0.04,
    width: width * 0.9,
  },
  inputContent: {
    paddingVertical: height * 0.01,
  },
  input: {
    fontSize: 15,
    width: width * 0.7,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  loginUser: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
  },
  picker: {
    fontSize: 10,
    color: '#aaa',
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
});
