import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/AntDesign';
import MostRecent from './MostRecent';
const {height, width} = Dimensions.get('window');
// import {useNavigation} from '@react-navigation/native';

export default function VacancyTab(params, props) {
  const Tab = [
    {
      Box: 'Most Relevant',
      icon: 'home-variant',
    },
    {
      Box: 'Most Recent',
      icon: 'account',
    },
  ];

  const [selectedBox, setSelectedBox] = useState('home-variant');
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const renderTabList = () => {
    if (selectedBox == 'home-variant') {
      return <MostRecent />;
    } else if (selectedBox == 'account') {
      return <MostRecent />;
    }
  };
  // const navigation = useNavigation();

  const renderTab = () => {
    return (
      <View style={{minHeight: '100%'}}>
        <View style={styles.headBox}>
          <Text style={styles.filterHeader}>Recent Vacancies</Text>
          <Text style={styles.button}>Show all</Text>
        </View>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.icon);
              }}
              style={[styles.tabBox]}>
              <Text
                style={[
                  styles.dotBox,
                  {color: item.icon === selectedBox ? '#2261a6' : '#fff'},
                ]}>
                ✿
              </Text>
              <Icon
                name={item.icon}
                color={item.icon === selectedBox ? '#2261a6' : '#d3d4d8'}
                size={30}
              />
            </TouchableOpacity>
          ))}
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              // backgroundColor: '#fff',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity>
              <Icon2 name={'pluscircle'} color={'#2261a6'} size={45} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentScroll}>
          {renderTabList()}
        </ScrollView>
      </View>
    );
  };

  return (
    <View style={{}}>
      <View style={{alignItems: 'center'}}>{renderTab()}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  filterHeader: {
    fontSize: 18,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  button: {
    fontSize: 18,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#9794a9',
  },
  headBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    elevation: 1,
    marginVertical: 20,
    // width: width - 40,
  },
  tabBox: {
    alignItems: 'center',
    // width: width * 0.4,
  },
  dotBox: {fontSize: 12, color: 'red'},
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
});
