import React, {useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.2;
const card_width = width * 0.6;
const box_width = width - 40;

export default function MostRecent(props) {
  const [like, setLike] = useState(true);
  const [product, setproducts] = useState([]);
  const [selectedRating, setSelectedRating] = useState('Facebook');
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  const earnings = [
    {
      id: 1,
      title: 'Facebook',
      job: 'Full time Ui Designer',
    },
    {
      id: 2,
      title: 'Google',
      job: 'Driver',
    },
    {
      id: 3,
      title: 'Instagram',
      job: 'Full time Ui Designer',
    },
    {
      id: 4,
      title: 'Boats',
      job: 'Full time Ui Designer',
    },
    {
      id: 5,
      title: 'Dragon',
      job: 'Graphics Designers',
    },
    {
      id: 6,
      title: 'Ride',
      job: 'Full time Ui Designer',
    },
  ];

  const Needs = () => {
    return earnings.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          TouchableHandler(item.title);
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor: item.title == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.name,
              {color: item.title == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.job}
          </Text>
          <Text
            style={[
              styles.introName,
              {color: item.title == selectedRating ? '#fff' : '#adabbb'},
            ]}>
            {item.title}
          </Text>
        </View>
        <View style={styles.right}>
          <Text
            style={[
              styles.bottomButtonText,
              {
                color: item.title == selectedRating ? '#fff' : '#aaa',
              },
            ]}>
            Freelance 5Hrs
          </Text>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={{}}>
      {Needs()}
      <View style={{height: height * 0.2}} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 0.1,
  },

  contentContairener: {
    width: box_width * 0.55,
  },

  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.4,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
