import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');
const Reward_Box_Height = width * 0.25;
const card_width = width * 0.6;
const box_width = width - 40;

export default function ApplicantListPage(props) {
  const [selectedRating, setSelectedRating] = useState('');
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };
  useEffect(() => {
    setSelectedRating(earnings[0].name);
  }, [500]);

  const earnings = [
    {
      id: 1,
      image:
        'https://cdn.pixabay.com/photo/2016/01/20/13/28/woman-1151562_960_720.jpg',
      name: 'Christeena MAriyam',
      experience: '3 Year experience',
    },
    {
      id: 2,
      image:
        'https://cdn.pixabay.com/photo/2015/01/06/16/14/woman-590490_960_720.jpg',
      name: 'Aysha Fathima',
      experience: '2 Year experience',
    },
    {
      id: 3,
      image:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      name: 'Dennis Dragon',
      experience: '3 Months Experiance',
    },
    {
      id: 4,
      image:
        'https://images.pexels.com/photos/3770317/pexels-photo-3770317.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      name: 'Angel Beccardi',
      experience: '1 Year experience',
    },
    {
      id: 5,
      image:
        'https://images.pexels.com/photos/4153629/pexels-photo-4153629.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      price: '30',
      name: 'Jyonikka Actrossia',
      experience: '2 Year experience',
    },
    {
      id: 6,
      image:
        'https://images.pexels.com/photos/3764119/pexels-photo-3764119.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      name: 'Kallyani',
      experience: '2 Year experience',
    },
  ];
  const Needs = () => {
    return earnings.map((item, index) => (
      <TouchableOpacity
        onPress={() => {
          TouchableHandler(item.name);
          props.navigation.navigate('ProfilePage');
        }}
        key={index}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor: item.name == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.left}>
          <View style={styles.imageContainer}>
            <Image source={{uri: item.image}} style={styles.productImage} />
          </View>
        </View>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.name,
              {color: item.name == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.name}
          </Text>
          <Text
            style={[
              styles.place,
              {
                color: item.name == selectedRating ? '#fff' : '#b0aebe',
              },
            ]}>
            {item.experience.length > 17
              ? item.experience.substring(0, 20) + '...'
              : item.experience}
          </Text>
        </View>
        <View style={styles.right}>
          <Text
            style={[
              styles.bottomButtonText,
              {
                color: item.name == selectedRating ? '#fff' : '#aaa',
              },
            ]}>
            5Hrs
          </Text>
        </View>
      </TouchableOpacity>
    ));
  };
  return (
    <ImageBackground
      source={require('../../../assets/vector-images/Group20260.png')}
      style={styles.backgroundImageBox}>
      <ScrollView contentContainerStyle={styles.ContentBox}>
        <View style={styles.TopContent}>
          <Text style={styles.companyName}>UI / UX Designer</Text>
          <View style={styles.detailBox}>
            <Text style={styles.locationText}>Freelance/</Text>
            <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
            <Text style={styles.locationText}>167 Koothattukulam</Text>
          </View>
          <View style={styles.contentNumberBox}>
            <Text style={styles.amount}>2000</Text>
            <Text style={[styles.amount, {fontSize: 12}]}>Applicants</Text>
          </View>
        </View>
        {Needs()}
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImageBox: {
    resizeMode: 'cover',
    width: width,
    minHeight: '100%',
  },
  contentNumberBox: {
    alignItems: 'center',
  },
  ContentBox: {
    paddingVertical: '15%',
    alignItems: 'center',
  },
  TopContent: {
    alignItems: 'center',
    marginBottom: 20,
  },

  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  amount: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 16,
  },
  companyName: {
    fontSize: 22,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#130937',
  },
  detailBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  locationText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#130937',
  },
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 1,
  },
  left: {
    alignItems: 'center',
    width: box_width * 0.2,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.5,
    width: Reward_Box_Height * 0.5,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
    borderRadius: 30,
  },
  contentContairener: {
    width: box_width * 0.5,
  },
  introName: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
    textTransform: 'capitalize',
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.16,
    height: Reward_Box_Height,
  },

  //   tablist style
});
