import React, {useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../header/BasicHeading';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.2;
const card_width = width * 0.6;
const box_width = width - 40;
import {useNavigation} from '@react-navigation/native';

export default function VacancyListPage(props) {
  const [like, setLike] = useState(true);
  const [product, setproducts] = useState([]);
  const [selectedRating, setSelectedRating] = useState();
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  useEffect(() => {
    setSelectedRating(earnings[0].job);
  }, []);
  const navigation = useNavigation();

  const earnings = [
    {
      id: 1,
      title: '30 Applicant',
      job: 'Full time Ui Designer',
    },
    {
      id: 2,
      title: '200 Applicant',
      job: 'Driver',
    },
    {
      id: 3,
      title: '500 Applicant',
      job: 'React Developer',
    },
    {
      id: 4,
      title: '2000 Applicant',
      job: 'Accountant',
    },
    {
      id: 5,
      title: '65 Applicant',
      job: 'Graphics Designers',
    },
    {
      id: 6,
      title: '55 Applicant',
      job: 'Java Developer',
    },
    {
      id: 1,
      title: '333 Applicant',
      job: 'Python Developer',
    },
    {
      id: 2,
      title: '509 Applicant',
      job: 'Angular Developer',
    },
    {
      id: 3,
      title: '22 Applicant',
      job: 'Basic Start Techie',
    },
    {
      id: 4,
      title: '10 Applicant',
      job: 'Content Designer',
    },
    {
      id: 5,
      title: '44 Applicant',
      job: 'Content Writer',
    },
    {
      id: 6,
      title: '33 Applicant',
      job: 'Basic Content Writer',
    },
  ];

  const Needs = () => {
    return earnings.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          props.navigation.navigate('ApplicationListPage');
          TouchableHandler(item.job);
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor: item.job == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.name,
              {color: item.job == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.job}
          </Text>
          <Text
            style={[
              styles.introName,
              {color: item.job == selectedRating ? '#fff' : '#adabbb'},
            ]}>
            {item.title}
          </Text>
        </View>
        <View style={styles.right}>
          <Text
            style={[
              styles.bottomButtonText,
              {
                color: item.job == selectedRating ? '#fff' : '#aaa',
              },
            ]}>
            Freelance 5Hrs
          </Text>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <>
      <View style={{paddingVertical: 30, paddingHorizontal: 10}}>
        <BasicHeading title="Vacancies" />
      </View>
      <ScrollView
        contentContainerStyle={{alignItems: 'center', paddingTop: 10}}>
        {Needs()}
        <View style={{height: height * 0.2}} />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 0.1,
  },

  contentContairener: {
    width: box_width * 0.55,
  },

  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.4,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
