import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
const {width, height} = Dimensions.get('window');

export default function PostVacancy(props) {
  const [category, setCategory] = useState('');
  const [subcategory, setSubcategory] = useState('');
  const [location, setLocation] = useState('');
  const [packages, setPackages] = useState('');
  const [time, setTime] = useState('');
  const [selectedRating, setSelectedRating] = useState('Contract');
  const bottomValue = useRef(new Animated.Value(100)).current;

  const picker1 = [
    {
      id: 1,
      label: 'Selected Category',
      value: 'UI/UX Design',
    },
    {
      id: 2,
      label: 'UI/UX Design',
      value: 'Offer',
    },
    {
      id: 3,
      label: 'Mobile App Developer',
      value: 'Veg',
    },
  ];
  const picker2 = [
    {
      id: 1,
      label: 'Graphics & Design',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: 'Animation Editor',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: 'Graphics for Content Maker',
      value: 'wwwwwwwwww',
    },
  ];
  const picker3 = [
    {
      id: 1,
      label: 'Belgin Germany',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: 'Kuruvatoor kozhikode',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: 'Edappally kochi',
      value: 'wwwwwwwwww',
    },
  ];
  const picker4 = [
    {
      id: 1,
      label: '2k-5k(yr)',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: '2k-9k(yr)',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: '4k-55k(yr)',
      value: 'wwwwwwwwww',
    },
  ];
  const picker5 = [
    {
      id: 1,
      label: 'Full Time',
      value: 'wwwwwwwwww',
    },
    {
      id: 2,
      label: 'Part Time',
      value: 'wwwwwwwwww',
    },
    {
      id: 3,
      label: 'Time Based',
      value: 'wwwwwwwwww',
    },
  ];

  const confirmHandler = (value) => {
    setSelectedRating(value);
  };
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 80000,
    }).start();
  };

  useEffect(() => {
    jumpUp();
  }, []);

  return (
    <View style={styles.middleContent}>
      <Animated.View
        style={[
          styles.contentBox,
          {
            transform: [{translateY: bottomValue}],
          },
        ]}>
        <Text style={styles.topHeader}>Post vacancies</Text>
        <View>
          <Text style={styles.headText}>Category</Text>
          <View style={styles.loginUser1}>
            <Icon name="bag-checked" size={25} color="#000" />
            <Picker
              selectedValue={category}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
              {picker1.map((item, index) => (
                <Picker.Item
                  key={index}
                  label={item.label}
                  value={item.value}
                />
              ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Sub Category</Text>
          <View style={styles.loginUser1}>
            <Icon name="bag-checked" size={25} color="#000" />
            <Picker
              selectedValue={subcategory}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) =>
                setSubcategory(itemValue)
              }>
              {picker2.map((item, index) => (
                <Picker.Item
                  label={item.label}
                  key={index}
                  value={item.value}
                />
              ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Location</Text>
          <View style={styles.loginUser1}>
            <Icon name="google-maps" size={25} color="#000" />
            <Picker
              selectedValue={location}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}>
              {picker3.map((item, index) => (
                <Picker.Item
                  label={item.label}
                  key={index}
                  value={item.value}
                />
              ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Packages</Text>
          <View style={styles.loginUser1}>
            <Icon name="package" size={25} color="#000" />
            <Picker
              selectedValue={packages}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setPackages(itemValue)}>
              {picker4.map((item, index) => (
                <Picker.Item
                  label={item.label}
                  key={index}
                  value={item.value}
                />
              ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Time</Text>
          <View style={styles.loginUser1}>
            <Icon name="clock-time-four" size={25} color="#000" />
            <Picker
              selectedValue={time}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setTime(itemValue)}>
              {picker5.map((item, index) => (
                <Picker.Item
                  label={item.label}
                  key={index}
                  value={item.value}
                />
              ))}
            </Picker>
          </View>
        </View>
        <View style={{width: width, paddingHorizontal: 20}}>
          <Text style={styles.headText}>Description</Text>
          <View style={styles.Description}>
            <Text style={styles.description}>
              Lorem Ipsum is dummy text used by publishers and designers when
              the original copy is not available.publishers and designers when
            </Text>
          </View>
        </View>
        <TouchableOpacity style={styles.login}>
          <Text style={styles.buttonText}>Post Now</Text>
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  middleContent: {
    width: width,
    alignItems: 'center',
    justifyContent: 'flex-end',
    minHeight: '100%',
    backgroundColor: 'transparent',
  },
  contentBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingVertical: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  topHeader: {
    fontSize: 20,
    marginBottom: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
  },

  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: width * 0.9,
    borderWidth: 1,
    borderColor: '#eee',
  },
  Description: {
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: width * 0.9,
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    borderColor: '#eee',
  },
  description: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    color: '#2d234e',
  },
  picker: {
    fontSize: 10,
    color: '#2d234e',
    width: width * 0.7,
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
    width: width - 40,
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
  },
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  itemFont: {
    fontFamily: 'BalooPaaji2-SemiBold',
    textAlign: 'center',
    fontSize: 16,
  },
  mainContainer: {
    paddingHorizontal: 8,
    paddingVertical: 15,
    borderRadius: 10,
    margin: 5,
    width: width * 0.25,
    backgroundColor: '#fafafa',
  },
  headText: {
    fontSize: 16,
    marginBottom: 10,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#130937',
  },
});
