import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import LogoScreen from '../screens/splash-screen/LogoScreen';
import IntroPage from '../screens/splash-screen/IntroPage';
import SignInPage from '../screens/Verification/SignInPage';
import Otp from '../screens/Verification/Otp';
import AccountTab from '../screens/Profile/AccountTab';
import SuccessPopup from '../screens/pop-up/SuccessPopup';
import Home from '../screens/home/Home';
import PostVacancy from '../screens/Post-vacancy/PostVacancy';
import VacancyListPage from '../screens/recent-vacancy/VacancyListPage';
import ApplicantListPage from '../screens/recent-vacancy/ApplicantListPage';
import ProfilePage from '../screens/Profile/ProfilePage';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default Base = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="LogoScreen"
          component={LogoScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="IntroPage"
          component={IntroPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignInPage"
          component={SignInPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OtpPage"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AccountTab"
          component={AccountTab}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SuccessPopUp"
          component={SuccessPopup}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PostVacancy"
          component={PostVacancy}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VacancyList"
          component={VacancyListPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ApplicationListPage"
          component={ApplicantListPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ProfilePage"
          component={ProfilePage}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});
