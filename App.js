import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import LogoScreen from './src/components/screens/splash-screen/LogoScreen';
import IntroPage from './src/components/screens/splash-screen/IntroPage';
import SignInPage from './src/components/screens/Verification/SignInPage';
import Otp from './src/components/screens/Verification/Otp';
import AccountTab from './src/components/screens/Profile/AccountTab';
import SuccessPopup from './src/components/screens/pop-up/SuccessPopup';
import Home from './src/components/screens/home/Home';
import PostVacancy from './src/components/screens/Post-vacancy/PostVacancy';
import VacancyListPage from './src/components/screens/recent-vacancy/VacancyListPage';
import ApplicantListPage from './src/components/screens/recent-vacancy/ApplicantListPage';
import ProfilePage from './src/components/screens/Profile/ProfilePage';
import Progress from './src/components/screens/Profile/Progress';
import Base from './src/components/navigation/Base';

const App = () => {
  return (
    <>
      {/* <StatusBar barStyle="dark-content" hidden={true} /> */}
      {/* <SafeAreaView> */}
      {/* <ProfilePage /> */}
      <Base />
      {/* <Progress title="Chakka" percentage={50} />
        <Progress title="Manga" percentage={70} /> */}
      {/* </SafeAreaView> */}
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
